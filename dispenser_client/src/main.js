import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'ag-grid-enterprise';
createApp(App).use(router).mount('#app')