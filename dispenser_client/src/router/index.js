import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

import PatientDiary from '../views/PatientDiary.vue'
import AddUser from '../views/AddUser.vue'
import NotFound from '../views/NotFound.vue'
import addMedToUser from '../views/addMedToUser.vue'
import addMed from '../views/addMed.vue'
import Alerts from '../views/Alerts.vue'
import Medication from '../views/Medication.vue'
import MedicalPatientDiary from '../views/MedicalPatientDiary.vue'
import Grid from '../views/Grid.vue'
const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },

    {
        path: '/patient-diary',
        name: 'PatientDiary',
        component: PatientDiary
    },
    {
        path: '/Medical-Patient-diary',
        name: 'MedicalPatientDiary',
        component: MedicalPatientDiary
    },
    {
        path: '/add-user',
        name: 'AddUser',
        component: AddUser
    },
    {
        path: '/add-med-to-user',
        name: 'addMedToUser',
        component: addMedToUser
    },

    {
        path: '/add-med',
        name: 'addMed',
        component: addMed
    },
    {
        path: '/alerts',
        name: 'alerts',
        component: Alerts
    },
    {
        path: '/medication',
        name: 'Medication',
        component: Medication
    },
    {
        path: '/grid',
        name: 'Grid',
        component: Grid
    },
    {
        path: '/:catchAll(.*)',
        name: 'NotFound',
        component: NotFound
    }

]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router