from json import dumps

from flask import Blueprint, request, Response, jsonify

from core import db

from .models import Patient, Medication, Medperuser, serialize, Alerts
from sqlalchemy import  and_

auth = Blueprint('auth', __name__)





@auth.route('/Add-Medication', methods=['GET', 'POST'])
def add_medication():
    if request.method == 'POST':
        med_name = request.json['name']
        comment = request.json['comments']
        status = int(request.json['status'])
        try:
            new_med = Medication(name=med_name, comments=comment, status=status)
            db.session.add(new_med)
            db.session.commit()
        except:
            return jsonify({'ok': False})
        return jsonify({'ok': True})


@auth.route('/delete-Medication', methods=["POST", "GET"])
def delete_Medication():
    if request.method == "POST":

        med_id = int(request.json['id'])

        try:
            medToDelete = Medication.query.filter_by(id=med_id).first()
            db.session.delete(medToDelete)
            db.session.commit()

        except Exception as ex:
            print(ex)
            return jsonify({'ok': False})
    return jsonify({'ok': True})


@auth.route('/delete-alert', methods=["POST", "GET"])
def delete_alert():
    if request.method == "POST":
        al_id = int(request.json['id'])

        try:
            allToDelete = Alerts.query.filter_by(id=al_id).first()
            db.session.delete(allToDelete)
            db.session.commit()

        except Exception as ex:
            print(ex)
            return jsonify({'ok': False})
    return jsonify({'ok': True})


@auth.route('/update-alert', methods=["POST", "GET"])
def update_alert():
    if request.method == "POST":
        al_id = int(request.json['id'])
        try:
            allToUpdate = Alerts.query.filter_by(id=al_id).first()
            allToUpdate.status = 1
            db.session.commit()

        except Exception as ex:
            print(ex)
            return jsonify({'ok': False})
    return jsonify({'ok': True})


@auth.route('/Add-alert', methods=['GET', 'POST'])
def add_alert():
    if request.method == 'POST':
        alert_type = request.json['type']
        user_id = request.json['userID']
        status = 0
        date = request.json['date']
        new_al = Alerts(user_id=user_id, alert_type=alert_type, status=status, date=date)
        db.session.add(new_al)
        db.session.commit()
    return jsonify({'ok': True})


@auth.route('/all-alerts')
def get_all_alerts():
    alert = db.session.query(Alerts).all()
    # convert to json
    serialized_data = [
        serialize(Alerts)
        for Alerts in alert
    ]
    jsonData = dumps(serialized_data)
    response = Response(
        response=jsonData,
        status=200,
        mimetype='application/json'
    )
    return response


@auth.route('/add-new-patient', methods=['GET', 'POST'])
def sigh_up():
    if request.method == 'POST':
        firstname = request.json['Name']
        lastrname = request.json['LName']
        gender =  request.json['gender']
        age = int(request.json['Age'])
        patID = int(request.json['patID'])
        try:
            new = Patient(firstname=firstname, lastname=lastrname, gender=gender, age=age, perID=patID)
            db.session.add(new)
            db.session.commit()
        except:
            return jsonify({'no ok': False})
        return jsonify({'ok': True})


@auth.route('/Add-Medication-to-patient', methods=['GET', 'POST'])
def add_medication_to_patient():
    if request.method == 'POST':
        user_id = int(request.json['userID'])
        med_name = request.json['medName']
        comments = request.json['comments']
        amount = int(request.json['amount'])
        instruction = request.json['conTime']
        ending_date = request.json['endingDate']

        try:
            new_med_user = Medperuser(med_name=med_name, comments=comments, amount=amount, instruction=instruction,
                                      ending_date=ending_date, user_id=user_id)
            db.session.add(new_med_user)
            db.session.commit()
        except:
            return jsonify({'ok': False})
        return jsonify({'ok': True})



@auth.route('/update-med-to-user', methods=['GET', 'POST'])
def update_amount_pill():
    if request.method == 'POST':

        new_amount = int(request.json['amount'])
        medId = int(request.json['id'])
        update_med_to_user = db.session.query(Medperuser).filter(Medperuser.id == medId).one()
        update_med_to_user.amount = new_amount
        db.session.commit()
    return jsonify({'ok': True})
@auth.route('/update-med-status', methods=['GET', 'POST'])
def update_status():
    if request.method == 'POST':
        med_id = int(request.json['id'])

        update_med_status = Medication.query.filter_by(id =med_id).first()
        update_med_status.status = 1
        db.session.commit()
    return jsonify({'ok': True})

@auth.route('/delete-med-to-user', methods=['GET', 'POST'])
def delete_med_to_user():
    if request.method == "POST":
        id = int(request.json['id'])

        try:

            TreatToDelete = Medperuser.query.filter_by(id=id).first()
            db.session.delete(TreatToDelete)
            db.session.commit()

        except Exception as ex:
            print(ex)
            return jsonify({'ok': False})
    return jsonify({'ok': True})

@auth.route('/delete-patient', methods=["POST", "GET"])
def delete_patient():
    if request.method == "POST":
        Per_id = int(request.json['perID'])

        try:
            PatToDelete = Patient.query.filter_by(perID=Per_id).first()
            db.session.delete(PatToDelete)
            db.session.commit()

        except Exception as ex:
            print(ex)
            return jsonify({'ok': False})
    return jsonify({'ok': True})




@auth.route('/get-meds-to-patient')
def get_meds_for_patient():
    if request.method == 'GET':
        per_id = request.args.get('PerID')

        patient = db.session.query(Medperuser).filter(Medperuser.user_id == per_id).all()
    # convert to json
        serialized_data = [
        serialize(Medperuser)
        for Medperuser in patient
        ]
        jsonData = dumps(serialized_data)
        response = Response(
            response=jsonData,
            status=200,
            mimetype='application/json'
        )
        return response
@auth.route('/get-all-meds-for-patients')
def get_meds_all_for_patients():

    patient = db.session.query(Medperuser).all()
    # convert to json
    serialized_data = [
        serialize(Medperuser)
        for Medperuser in patient
    ]
    jsonData = dumps(serialized_data)
    response = Response(
        response=jsonData,
        status=200,
        mimetype='application/json'
    )
    return response

@auth.route('/all-patients')
def get_all_patients():
    patients = db.session.query(Patient).all()

    # convert to json
    serialized_data = [
        serialize(Patient)
        for Patient in patients
    ]
    jsonData = dumps(serialized_data)

    response = Response(
        response=jsonData,
        status=200,
        mimetype='application/json'
    )
    return response


@auth.route('/all-Medication')
def get_all_Medication():
    Meds = db.session.query(Medication).all()
    db.session.remove()

    # convert to json
    serialized_data_med = [
        serialize(Medication)
        for Medication in Meds
    ]
    jsonData = dumps(serialized_data_med)

    response = Response(
        response=jsonData,
        status=200,
        mimetype='application/json'
    )
    return response
