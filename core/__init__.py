from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

db = SQLAlchemy()

def create_app():
    from core.views import views
    from core.auth import auth

    app = Flask(__name__)
    CORS(app)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config[
        'SQLALCHEMY_DATABASE_URI'] = 'postgresql://root:orARIEL10@database-3.c9gdecclk8ay.us-east-1.rds.amazonaws.com/med_db'
    app.config["SQLALCHEMY_ECHO"] = True

    db.init_app(app)

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')

    # from .models import Patient
    # from .models import Medication
    # from .models import Medperuser
    # from .models import Alerts

    db.create_all(app=app)

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    return app
