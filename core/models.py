from sqlalchemy import func
from . import db


from json import dumps
from sqlalchemy.orm import class_mapper

class Patient(db.Model):
    perID = db.Column(db.Integer,primary_key=True,nullable=False)
    firstname = db.Column(db.String(50))
    lastname = db.Column(db.String)
    gender = db.Column(db.String)
    age = db.Column(db.Integer)
    med_per_patient = db.relationship('Medperuser')


class Medication(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    comments = db.Column(db.String)
    status = db.Column(db.String)


class Medperuser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    med_name = db.Column(db.String(150))
    instruction = db.Column(db.String(500))
    ending_date = db.Column(db.String)
    comments = db.Column(db.String(500))
    amount = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('patient.perID'))


class Alerts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    alert_type = db.Column(db.String(500))
    date = db.Column(db.String(500))
    status = db.Column(db.String(500))

def serialize(model):
  """ https://stackoverflow.com/questions/14920080/how-to-create-sqlalchemy-to-json """

  """Transforms a model into a dictionary which can be dumped to JSON."""
  # first we get the names of all the columns on your model
  columns = [c.key for c in class_mapper(model.__class__).columns]
  # then we return their values in a dict
  return dict((c, getattr(model, c)) for c in columns)